<?php  
$theme_url = get_template_directory_uri(); $site_url = get_site_url(); 

	function echo_thumbnail(){
		global $post;
        if(get_post_type($post) == 'news')
        {
            $thumbnail = get_post_meta($post->ID, 'news_item_thumbnail', true);
            if($thumbnail && strlen($thumbnail) > 0)
            {
                $img_src = wp_get_attachment_image_src($thumbnail);
                echo '<meta name="thumbnail" content="'.$img_src[0].'" />';
				
            }
        }
	}
	global $paged, $page,$post;
    function show_robots($post){
		if( !empty($post)){
			$template = explode('.', get_post_meta( $post->ID, '_wp_page_template', true ));
			if(((!empty($template[0])&& $template[0] == 'news_archive') || $post->post_type == 'news')){

				$limitations = get_option('display_limitations_on_news');
				$display_limitations = get_option('display_limitations');
				$post_date = $post->post_date;
				$today = date("Y-m-d H:i:s");
				if($limitations == 'limit' && !empty($display_limitations)){
					$postdate = strtotime($post_date);
					$limitday = strtotime($today.'-'.$display_limitations.' month');
					if($postdate < $limitday){
						echo '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW, NOARCHIVE">';
					}
				}
			}
		}
}

$logo_settings = get_option( 'chlt_option_name' );
//print_r($logo_settings);die();
$site_url = get_site_url();
$path_info = pathinfo($site_url);
$home_url = esc_url( home_url( '/' ) ) ;
/* font from admin settings */
// if input is not empty take font from admin settings
    //exclude monitors traffic from GA
function end_head_tags($post){  
   $exclude_users_agents = '';
    if (file_exists(WP_PLUGIN_DIR . '/ga-traffic-filtering/settings.txt')) {
        $exclude_users_agents = @file_get_contents(WP_PLUGIN_DIR . '/ga-traffic-filtering/settings.txt');
    }
    if (isset($_SERVER['HTTP_USER_AGENT']) && !preg_match('/'.$exclude_users_agents.'/i', $_SERVER['HTTP_USER_AGENT']))
        if ( function_exists( 'yoast_analytics' ) )
            yoast_analytics();
    do_action('wcms_scripts','end_of_head_tag_script');
	do_action('wcms_scripts', 'end_of_head_script');
	}
	
    $p_type = get_post_type($post);
    $page_type = $p_type == 'page' ? pathinfo(get_page_template(), PATHINFO_FILENAME) : $p_type;
    

    
			function social_li($site_url){
				echo '<li><a target="_blank" href="http://www.facebook.com/sharer.php?u='.$site_url.'" class="facebook"><i class="fa fa-facebook"></i></a></li>
					<li><a target="_blank" href="https://plus.google.com/share?url='.$site_url.'" class="google"><i class="fa fa-google-plus"></i></a></li>
					<li><a target="_blank" href="http://twitter.com/share?text=An%20Awesome%20Link&url='.$site_url.'" class="twitter"><i class="fa fa-twitter"></i></a></li>';
			}
			
			function show_menu(){		
                        $menu = wp_cache_get('top_menu_'.$post->ID);
                        if (false === $menu) {
                            $menu = wp_nav_menu(array('echo' => false,'menu'  => 'top_menu', 'container' => false, 'walker' => new themeslug_walker_nav_menu));
                            wp_cache_add('top_menu_'.$post->ID,$menu,false,3600);
                        }
                        echo $menu;
			}
			function return_menu(){
                global $post;
                        $menu = wp_cache_get('top_menu_'.$post->ID);
                        if (false === $menu) {
                            $menu = wp_nav_menu(array('echo' => false,'menu'  => 'top_menu', 'container' => false, 'walker' => new themeslug_walker_nav_menu));
                            wp_cache_add('top_menu_'.$post->ID,$menu,false,3600);
                        }
                        return $menu;
			}
			

$context = Timber::get_context();
$context['env'] = ENV;
$context['display_search_form']='display_search_form';
$context['']='nothing';
$context['Share']='Share';
$context['Menu']='Menu';
$context['html5_doctype']='<!doctype html>';
$context['display_custom_logo']='display_custom_logo';
$context['page_type']=$page_type;
$context['g_post']=$post;
$context['fbid']='293050607410339';
$context['socialLocalization']='sv_SE';
$context['wcms_scripts']='wcms_scripts';
$context['a_b_script']='a_b_script';
$context['body_tag_script']='body_tag_script';
$context['pipe']='|';
$context['true_symbol']=true;
$context['right']='right';
$context['header_display_follow']=header_display_follow;
$context['logo_img'] = $logo_settings['image_logo'];
$context['logo_alt'] = get_settings_logo_alt($context['logo_img']);

$context['site_url']=get_site_url();
$context['path_info']=pathinfo($site_url);
$context['post'] = new TimberPost();
$context['facebook'] = get_option('fb_acc');
$context['twitter'] = get_option('twitter_acc');
$context['googleplus'] = get_option('gplus_acc');
$context['translation1'] = __('Back','red');
$context['facebook'] = get_option('fb_acc');
$context['twitter'] = get_option('twitter_acc');
$context['googleplus'] = get_option('gplus_acc');
$context['menu'] = return_menu();
$context['home_url'] = $home_url;
Timber::render(array('header.twig'), $context);
?>
		