<?php
/**
 * Adding translation to ACF values :
 *  1. Add the phrase in current file.
 *  2. Make sure to use the correct method to print the value, ex: _e( $merged_product_object['slot_type'], 'ACF Translations' ) .
 *  3. Rescan the location of this file. In case this file exist in Red theme:  Admin -> WMPL -> Theme and plugins localization -> Scan the theme for strings
 *  4. Set the translation in Admin -> WPML -> String Translation
 */

__( 'Video Slot', 'ACF Translations'  );
__( 'Video Poker', 'ACF Translations'  );
__( 'Progressive', 'ACF Translations'  );
__( '3D Slot', 'ACF Translations'  );
__( 'Pooled jackpot', 'ACF Translations'  );
__( 'Progressive jackpot', 'ACF Translations'  );
__( 'Fixed jackpot', 'ACF Translations'  );
__( 'Other', 'ACF Translations'  );
__( 'None', 'ACF Translations'  );
__( 'Yes', 'ACF Translations'  );
__( 'yes', 'ACF Translations'  );
__( 'No', 'ACF Translations'  );
__( 'no', 'ACF Translations'  );
__( 'Instant Play', 'ACF Translations'  );
__( 'Download', 'ACF Translations'  );
__( 'Live Casino', 'ACF Translations'  );

//These translations were inserted in order to support blocked brands function (WCMS-2196)
__( 'Play now at', 'Red'  );
__( 'Try this legal casino', 'Red'  );

__( 'Visit', 'Red'  );
__( 'Visit this legal casino', 'Red'  );

__( 'Click to Play at', 'Red'  );
__( 'Play now at this legal casino', 'Red'  );

__( 'Play at *** now','Red');
__( 'Play now at this legal casino ***','Red');


__( 'Play these games and more at *** now', 'Red'  );
__( 'Play these games at this legal casino ***', 'Red'  );

__( 'Click to Play at', 'Red'  );
__( 'Click to Play at this legal casino ***', 'Red'  );

__( 'at ***', 'Red'  );
__( 'at this legal casino ***', 'Red'  );

__( 'Click to Play at ***', 'Red'  );
__( 'Play now at this legal casino ***', 'Red'  );

__( 'Visit ***', 'Red'  );
__( 'Visit this legal casino ***', 'Red'  );
//END of block brand's translations