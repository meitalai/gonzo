/**
 * Created by adi on 06/01/2015.
 */
// global vars

var queryPortrait = "screen and (orientation: portrait)",
    queryLandscape = "screen and (orientation: landscape)",
    queryMobileToTablet = "screen and (max-width: 991px)",
    queryMobile = "screen and (max-width: 767px)",
    queryTablet = "screen and (min-width: 768px) and (max-width: 991px)",
    queryTabletToDesktop = "screen and (min-width: 768px)",
    query2break = "screen and (min-width: 992px) and (max-width: 1199px)",
    queryDesktop = "screen and (min-width: 1200px)",
    queryAllDesktop = "screen and (min-width: 992px)",
    currentQuery = "";


$(document).ready(function(){
       
    /* Media Queries enquire */
    enquire
        .register(queryAllDesktop, {
            setup : function() {


                if(isMobile.any()){
                    //social
                    $('.social').addClass('mobile');
                    $('.social').on('click', function(){
                        $(this).toggleClass('open');
                    });

                }

                $('#menu-top_menu').mainResponsiveMenu('#mainNavigation','#navToggle', 'fa-caret-down','fa-angle-left' ).init();

            },
            match : function() {
                $('#menu-top_menu').mainResponsiveMenu('#mainNavigation','#navToggle').match(false);
                $('body').addClass('desktop');

            },
            unmatch : function() {

                $('body').removeClass('desktop');

            }

        })
        .register(queryPortrait, {
            match : function() {

            }
        })
        .register(queryLandscape, {
            match : function() {

            }
        })
        .register(query2break, {
            match : function() {
                $('body').addClass('secondBreak');
                $('#menu-top_menu').moreMenu().match(true);

            },
            unmatch : function() {
                $('body').removeClass('secondBreak');
            }
        })
        .register(queryDesktop, {
            match : function() {
                $('#menu-top_menu').moreMenu().match(true);
            },
            unmatch : function() {
            }
        })
        .register(queryMobileToTablet, {
            match : function() {
                $('body').addClass('mobile');
                $('#menu-top_menu').mainResponsiveMenu('#mainNavigation','#navToggle').match(true);
                $('#menu-top_menu').moreMenu().match(false);


            },
            unmatch : function() {
                $('body').removeClass('mobile');

            }
        })
        .register(queryTablet, {
            match : function() {

                $('body').addClass('tablet');
                // collapse only on tablet
                setTimeout(function(){
//                console.log('in tablet');
                    $('.msg_head.tablet').expand(".addReadMore", "default",600, false, true,"fa fa-chevron-up","fa fa-chevron-down");
                    $(".addReadMore").not('.default').hide();

                }, 0);



            },
            unmatch : function() {
                $('body').removeClass('tablet');

                $('.msg_head.tablet').expand(".addReadMore", "default",600, true, true);
//                console.log('out tablet');
            }
        })
        .register(queryTabletToDesktop, {
            match : function() {
                $('body').addClass('notMobile');


            },
            unmatch : function() {
                $('body').removeClass('notMobile');
            }
        })
        .register(queryMobile, {
            match : function() {

                // collapse only on mobile
                $('.addReadMore').addClass('readMore').hide();
                $('.msg_head').expand(".readMore", "default.",600, false, true,"fa fa-angle-up","fa fa-angle-down");
                $(".readMore").not('.default').hide();

                // screenshots
                $('a.ImgOverlay').ImgOverlay(true, false);

            },
            unmatch : function() {
                $('a.ImgOverlay').ImgOverlay(true, true);


                $('.msg_head').expand(".readMore", "default",600, true, true);
                $('.addReadMore').removeClass('readMore');
            }
        });

//    end

    function callDonutChart(){
        $(".donutchart").destroydonutPie();
        enquire
            .register(queryTabletToDesktop, {
                match : function() {

                },
                unmatch : function() {

                }
            })
            .register(queryMobile, {
                match : function() {

                },
                unmatch : function() {

                }
            })
    };

});

function fontLoad_callback() {
    if (!$('body').hasClass('mobile')) {
        $('#menu-top_menu').moreMenu().match(true)
    };
};