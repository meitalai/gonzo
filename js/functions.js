(function ($) {

	 //open screenshot overlay
	  $.fn.ImgOverlay = function(kill, click){
	  		if (kill){
	  			$(this).unbind('click');
	  		}else{
  				if ($(this).hasClass('hover') && !click ){
					$(this).off('click');
					//hover screenshot overlay
					$(this).each(function(){
                        $(('.popImg'),this).remove();
	  			 		$(this).attr('href',$(this).attr('data-href'));
					    var ImgSrc = $(this).find('img').attr('src');
					    var playAt = $(this).attr('data-hover');
						$(this).append('<div class="popImg  hidden-mobile hidden-tablet"><img src="'+ImgSrc+'" alt=""><span class="playAt">' +playAt + '</span></div>');
						 $('.popImg').css('display','none');
					})

					$(this).on('mouseenter', function() {
						$('.popImg', this).css('display','block');
					    return false;
				    });
		  			$(this).on('mouseleave', function() {
						$('.popImg', this).css('display','none');
					    return false;
		  			});

	  			}else {

  					$(this).attr('href','');
                    $(this).on('click', function(e) {
						    var ImgSrc = $(this).find('img').attr('src');
						    var aHref = $(this).attr('data-href');
						    var playAt = $(this).attr('data-hover');
						    $('.overlay').fadeIn();
						    $('.popup').find('img').attr('src',ImgSrc);
							console.log( $('.popup').find('img'));
						    $('.popup').find('a.conv').attr('href',aHref);
						    $('.popup').find('.playAt').text(playAt);
						    $('.popup').fadeIn();
						    e.preventDefault();
			  		  });
					$('.overlay, .close').on('click',function(e){
						e.preventDefault();
						$('.popup').fadeOut();
						$('.overlay').fadeOut();

					});

				}
  		}
	}

	$.fn.mainResponsiveMenu = function(navCont, navToggle, arrowSub, backArrow){
		var thisMenu = this,
			$subMenus = thisMenu.find('li').has('ul'),
			dataBack = $(navCont).attr('data-back'),
			backBtn = '<li class="back"><a href="#">' + dataBack + '</a><span class="ddArrow"><i class="fa ' + backArrow + '  "></i></span></li>',
			$navCont = $(navCont),
			$toggleBtn = $(navToggle);

		function closeSubMenu($thisSubMenu){
			if ($thisSubMenu.parents('ul.sub-menu').length) {
				$thisSubMenu.parents('ul.sub-menu').removeClass('open');
			}else{
				$navCont.removeClass('subOpen');
			}
//			console.log($thisSubMenu.length);
			$thisSubMenu.parent('li').removeClass('active');
		}
		function openSubMenu($thisSubMenu){
			if ($thisSubMenu.parents('ul.sub-menu').length)
				$thisSubMenu.parents('ul.sub-menu').addClass('open');
//				console.log($thisSubMenu);
			$thisSubMenu.siblings('ul').removeClass('active')
			$thisSubMenu.addClass('active');
			$navCont.addClass('subOpen');
		}
        function closeMenu(){

            $navCont.removeClass('active');
            $subMenus.removeClass('active').find('ul.sub-menu').removeClass('open');
            $('body').removeClass('noScroll');
            $(window).scrollTop(topBodyScroll);
            if ($navCont.hasClass('subOpen')) $navCont.removeClass('subOpen');
            if($('#overlayNav').length) {
                $('#overlayNav').remove();
            }
        }
        function openMenu(){
            topBodyScroll = $(window).scrollTop();
            topBody = topBodyScroll  * (-1) ;
            $('body').css('top',(topBody));
            $('body').addClass('noScroll');
            $('body').append('<div id="overlayNav"></div>');
            $navCont.addClass('active');
            $('#overlayNav').on('click', function(){
                closeMenu();
            });
        }
		return {
			init: function(){
				$subMenus
					.addClass('sub')
					.children('ul.sub-menu').prepend(backBtn);
				$toggleBtn.on('click',function(){
//					console.log('123');
					if ($navCont.hasClass('active')){
						closeMenu();
					}else{
						openMenu();
					}
				});

				var arrow = '<span class="ddArrow"><i class="fa ' + arrowSub + ' "></i></span>';
				$(arrow).appendTo($subMenus.children('.wrapBtn'));
				$('li.back').on('click',function(){
					closeSubMenu($(this).closest('ul.sub-menu'));
				});

				if (!isMobile.any()) thisMenu.addClass('desktop');

			},
			match: function(isMobileMenu){
//				console.log('match menu');
				$navCont.on('click',function(e){
					if ((!$(e.target).hasClass('sub-menu') || !$(e.target).closest('.sub-menu').length) && $(this).hasClass('subOpen')){
//						console.log('ahhhhhhh');
					}
				});
				$subMenus.each(function(index) {
					$(this).removeClass('active');
				});
				if (!isMobileMenu){//if isn't mobile menu
					thisMenu.find('.ddArrow').off('click');
					if (isMobile.any()) {

						$navCont.addClass('mobileDevice');

						thisMenu.find('.ddArrow').on('click',function(){
//							console.log($(this));
							$(this).closest('li.sub').siblings('li.active').removeClass('active');
							$(this).closest('li.sub').toggleClass('active').find('li.active').removeClass('active');
						});

					}else{
						if($('#overlayNav').length) $('#overlayNav').remove();
						if ($navCont.hasClass('active')) $navCont.removeClass('active');
						if ($navCont.hasClass('subOpen')) $navCont.removeClass('subOpen');
						$subMenus.each(function(index) {
							var $thisSubMenu = $(this);
							//$thisSubMenu.find('.ddArrow').off('click');
						});
					}

				}else{ //if is mobile menu
					$navCont.removeClass('mobileDevice');
					$subMenus.each(function(index) {
						var $thisSubMenu = $(this);
						$thisSubMenu.find('.ddArrow').off('click');
						$thisSubMenu.find('.ddArrow')
							.on('click', function(){
								if ($thisSubMenu.hasClass('active')){
									closeSubMenu($thisSubMenu);
								}else{
									openSubMenu($thisSubMenu);
								}
							});
					});
				}

			}
		}
	}

	$.fn.moreMenu = function(){
		var $thisMenu = $(this);

		return{
			match: function(needsMoreMenu){
				$thisMenu.append($thisMenu.find('li.hideshow ul').html());
				$thisMenu.children('li.hideshow').remove();
				if (needsMoreMenu){
					alignMenu();
					checkChildren();
				}
			}
		}

		function alignMenu() {
			var w = -1;
			var mw = $thisMenu.width() - $thisMenu.children('li').width();
			var i = -1;
			var menuhtml = '';
			$.each($thisMenu.children(), function() {
				i++;
				w += $(this).outerWidth(true);
				if (mw < w) {
					menuhtml += $('<div>').append($(this).clone()).html();
					$(this).remove();
				}
			});
			$thisMenu.append(
					'<li  style="position:relative;" href="#" class="hideshow">'
					+ '<div class="more"><i class="fa fa-circle"></i> <i class="fa fa-circle"></i> <i class="fa fa-circle"></i> '
					+ '</div><ul>' + menuhtml + '</ul></li>');

			if (isMobile.any()) {
				$('.more', $thisMenu).on('click', function () {
					$('li.active',$thisMenu).not('.hideshow').removeClass('active');
					$(this).parent().toggleClass('active');
					$(this).parent().find('.active').removeClass('active');
				});
				$('.hideshow .ddArrow', $thisMenu).on('click', function () {
					$(this).closest('li').toggleClass('active').siblings().removeClass('active');
				});
			}else{
				('.hideshow', $thisMenu).removeClass('active');
			}
//        $("#horizontal li.hideshow ul").css("bottom",  $("#horizontal li.hideshow").outerHeight(true) + "px");
//			$thisMenu.children('li.hideshow').on('mouseenter', function() {
//				$(this).children("ul").toggle();
//			}).on('mouseleave',function(){
//				$(this).children("ul").toggle();
//			});
		};

		function checkChildren(){
			if($thisMenu.find('li.hideshow ul').children().length > 0){
//            console.log('big'+ $("#horizontal li.hideshow ul").children().length);
				$thisMenu.children('.hideshow').css('display', 'inline-block');
			} else {
//            console.log('small'+ $("#horizontal li.hideshow ul").children().length);
				$thisMenu.children('.hideshow').css('display', 'none');
			}
		};

	}

}(jQuery));

