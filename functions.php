<?
if(!function_exists('modify_jquery')){
    function modify_jquery() {
        
            // comment out the next two lines to load the local copy of jQuery
            $bloginfo = get_stylesheet_directory_uri();
            wp_deregister_script('jquery');
            wp_register_script( 'jquery', $bloginfo.'/js/jquery-2.1.1.min.js', array(), true);
            wp_enqueue_script('jquery');
        
    }
}

add_action('init', 'modify_jquery');

if(!function_exists('lt_load_scripts')) {
    function lt_load_scripts() {
        $bloginfo = get_stylesheet_directory_uri();
       // wp_enqueue_style( 'bootstrap', $bloginfo.'/css/bootstrap.min.css');

       // wp_enqueue_style( 'slick', $bloginfo.'/css/slick.css');
       wp_enqueue_style( 'style', $bloginfo.'/style.css',array('wcms_widgets-style'));
        wp_enqueue_style( 'style-default', $bloginfo.'/css/skins/style-blue.css',array('wcms_widgets-style'));

       // wp_enqueue_script( 'slick', $bloginfo.'/js/slick.js', array('jquery'),'', true);
       // wp_enqueue_script( 'enquire', $bloginfo.'/js/enquire.min.js', array(),'', true);
        wp_enqueue_script( 'functions', $bloginfo.'/js/functions.js', array('jquery','widgets_scripts_1'),'', true);
        wp_enqueue_script( 'scripts', $bloginfo.'/js/scripts.js', array('jquery','enquire','functions'),'', true);
        //wp_enqueue_script( 'scriptsrnd', $bloginfo.'/js/scripts-rnd.js', array('jquery','enquire','functions'),'', true);


       $options = get_option( 'scripts_option_name' );
        if(defined('MULTISITE') && MULTISITE  ){
            $site_Synchronizes = get_site_url( 1 ).'/wp-content/uploads/wcms_sites_css';
        }else{
            $site_Synchronizes = site_url().'/wp-content/uploads/wcms_sites_css';
        }
        if(!empty( $options['disable_site_style'] )){
            wp_enqueue_style( 'site_style_css', $site_Synchronizes.'/site_style.css',array('wcms_widgets-style','style-default', 'style'),'','all', true);
            wp_enqueue_script( 'site_script_js', $site_Synchronizes.'/site_scripts.js', array(),'', true);
        }


    }
}

add_action('wp_enqueue_scripts', 'lt_load_scripts');

class themeslug_walker_nav_menu extends Walker_Nav_Menu {

        // add classes to ul sub-menus
        function start_lvl(&$output, $depth = 0, $args = array()) {


            // depth dependent classes
            $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent

            // build html
            $output .= "\n" . $indent . '<ul class="sub-menu">' . "\n";

        }

        function end_lvl( &$output, $depth = 0, $args = array()) {

            $depth = is_object($depth) ? $depth->depth : $depth;
            $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent

            // build html
            $output .= "\n" . $indent . '</ul>' . "\n";
        }

        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )  {

            $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
			$classes = $item->classes;
			$classes_string = implode(' ' ,$classes);
            if(in_array('menu-item-has-children',$item->classes)){
				
                $output .= "\n" . $indent . '<li class="'.$classes_string.'"><div class="wrapBtn"><a href="'.$item->url.'">'.__($item->title,'gonzo').'</a></div>' . "\n";
            }

            else{
                $output .= "\n" . $indent . '<li class="'.$classes_string.'"><div class="wrapBtn"><a href="'.$item->url.'">'.__($item->title,'gonzo').'</a></div>' . "\n";
            }

        }

        function end_el( &$output, $item, $depth = 0, $args = array() ) {

            if($item->title == 'brands')
                $output .= "</li>\n";
            else
                parent::end_el($output, $item, $depth, $args);
        }
    }

/**
 * add an option to appearance -> customize will add three checkboxes under social options
 * @param $wp_customize
 */
function default_social_customizer( $wp_customize ) {
    $wp_customize->add_section(
        'Gonzo_social_section',
        array(
            'title' => 'Social options',
            'description' => 'Control Social options for the Gonzo theme',
            'type' => 'option',
            'priority' => 35,
        )
    );

    $wp_customize->add_setting(
        'display_social_buttons_on_news_posts',
        array(
            'default' => true,
        )

    );
    $wp_customize->add_setting(
        'display_social_buttons_on_article_pages',
        array(
            'default' => true,
        )
    );
    $wp_customize->add_setting(
        'display_social_buttons_on_blog_posts',
        array(
            'default' => true,
        )
    );

    $wp_customize->add_control(
        'display_social_buttons_on_news_posts',
        array(
            'type' => 'checkbox',
            'label' => 'Display social buttons on News posts',
            'section' => 'Gonzo_social_section',

        )
    );
    $wp_customize->add_control(
        'display_social_buttons_on_article_pages',
        array(
            'type' => 'checkbox',
            'label' => 'Display social buttons on Article pages',
            'section' => 'Gonzo_social_section',
        )
    );
    $wp_customize->add_control(
        'display_social_buttons_on_blog_posts',
        array(
            'type' => 'checkbox',
            'label' => 'Display social buttons on Blog posts',
            'section' => 'Gonzo_social_section',
        )
    );

}
add_action( 'customize_register', 'default_social_customizer' );
/**
 * this function will decide if to display share options based on the apperance->customize->social options
 * @return html string for the share option
 */
function add_share_box(){
$display_social_buttons_on_blog_posts = get_theme_mod( 'display_social_buttons_on_blog_posts' );
$display_social_buttons_on_news_posts = get_theme_mod( 'display_social_buttons_on_news_posts' );
$display_social_buttons_on_article_pages = get_theme_mod( 'display_social_buttons_on_article_pages' );

    $html = '';
    $settings = get_option( 'share_options' );
    $site_url = get_site_url();
    $decision = true;

    if(get_post_type() == 'blog_posts' && !$display_social_buttons_on_blog_posts){
        $decision = false;
    }
    if(get_post_type() == 'news' && !$display_social_buttons_on_news_posts){
        $decision = false;
    }
    if(get_page_template_slug() == 'article.php' && !$display_social_buttons_on_article_pages){
        $decision = false;
    }

    if($settings && $decision)
    {
        $settings = json_decode($settings, true);
        $html ='<div class="shareBtns col-md-3 visible-desktop pull-right " data-share="Share this">
                        <!-- AddThis Button BEGIN -->
                        <ul>';
        if($settings['facebook'])
            $html .= '<li class="col-xs-2"><a title="Share this on Facebook" href="http://www.facebook.com/sharer.php?u='.$site_url.'" class="fb"><i class="fa fa-facebook"></i></a></li>';
        if($settings['google+'])
            $html .= ' <li class="col-xs-2"><a title="Share this on Google" href="https://plus.google.com/share?url='.$site_url.'" class="gp"><i class="fa fa-google-plus"></i></a></li>';
        if($settings['twitter'])
            $html .= ' <li class="col-xs-2"><a title="Share this on Twitter" href="http://twitter.com/share?text=An%20Awesome%20Link&url='.$site_url.'" class="tw"><i class="fa fa-twitter"></i></a></li>';

        $html .= ' <li class="share">'.__('Share this', 'casino').'</li></ul></div>';
    }

    return $html;
}


?>