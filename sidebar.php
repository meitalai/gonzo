<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Lighttemolate
 * @since Lighttemolate 1.0
 */

global $post;

$custom_fields = get_post_custom();
//echo '<pre>'; var_dump();exit;
$sidebar = $custom_fields["_sidebar"][0];
$sidebar2 = $custom_fields["_sidebar2"][0];

?>
<aside id="sidebar" class="sidebar col-xs-12">
    <div class="row">
        <?
        if($sidebar == 'Select Sidebar' || empty($sidebar)){

            $page_query = get_queried_object();
            $page_id = $page_query->ID;

            //check page or post
            if(is_page($page_id)){
                $template_slug = get_post_meta( $page_id, '_wp_page_template', true );
                $templates = wp_get_theme()->get_page_templates();

                // if have parent get parent id and check if parent has sidebar
                if($post->post_parent){
                    $parent_id = $post->post_parent;
                    $template_slug = get_post_meta( $parent_id, '_wp_page_template', true );
                }
                else{
                    $sidebar = 'Home Page';
                }
                foreach ( $templates as $template_filename => $template_name  ) {
                    if ( $template_filename == $template_slug ){
                        $sidebar = $template_name;
                    }
                    elseif($template_slug == 'default'){
                        $sidebar = 'Home Page';
                    }
                }
            }
            else{

                // TO DO: CHECK IF taxonomy exist
                $post_type = get_post_type($page_id);

                if($post_type == 'news'){
                    $sidebar = $post_type;
                }
                elseif(is_404()){

                    $sidebar = '404 page';
                }
                else{
                    $sidebar = 'Article';
                }
            }
            dynamic_sidebar($sidebar);

        }
        else{
            // ------user selected sidebar------//
            dynamic_sidebar($sidebar);
        }

        if(!empty($sidebar2) && $sidebar2 != 'Select Sidebar'){
            dynamic_sidebar($sidebar2);
        }
        ?>
    </div>
</aside>