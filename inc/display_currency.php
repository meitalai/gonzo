<?php
/**
 * Implement an display currency for Light Template
 * in general options IN CMS
 * @package WordPress
 * @subpackage Light_Template
 * @since  Light Template
 */

 $lt_currency_general_setting = new lt_currency_general_setting();

class lt_currency_general_setting {
	
	public function __construct() {
		add_filter( 'admin_init' , array( &$this , 'register_fields' ) );
	}
	public function register_fields() {
		register_setting( 'general', 'display_currency', 'esc_attr' );
		add_settings_field('disp_curr', '<label for="display_currency">'.__('How to display currency?' , 'display_currency' ).'</label>' , array(&$this, 'fields_html') , 'general' );
	}
	public function fields_html() {
		$dc = '';
		$value = get_option( 'display_currency', '' );
		
		$dc .= '<lable title="before">';
		$dc .= '<input type="radio" id="display_currency" name="display_currency" value="before" '.(($value == "before") ? "checked" : "").'/>';
		$dc .= '<span>'.__('Display currency before','Red').'</span>';
		$dc .= '</lable>';
		$dc .= '<br><br>';
		$dc .= '<lable title="after">';
		$dc .= '<input type="radio" id="display_currency" name="display_currency" value="after" '.(($value == "after") ? "checked" : "").'/>';
		$dc .= '<span>'.__('Display currency after','Red').'</span>';
		$dc .= '</lable>';
		
		echo $dc;
	}
}